"""
Define MDRNN model, supposed to be used as a world model
on the latent space.
"""
import math

import torch
import torch.nn as nn
import torch.nn.functional as f


U_DIM = 32
latent_space =32
num_of_guassians = 5
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class NanLossError(Exception):

    def __init__(self, batch,mus,sigmas,logpi,U, message="loss became NaN"):
        torch.save(batch,'./batch.pt')
        torch.save(mus, './mus.pt')
        torch.save(sigmas, './sigmas.pt')
        torch.save(logpi,'./logpi.pt')
        torch.save(U, './U.pt')
        self.message = message
        super().__init__(self.message)

class NegativeLossError(Exception):
    def __init__(self, batch,mus,sigmas,logpi,U, message="loss became Negative"):
        torch.save(batch,'./batch.pt')
        torch.save(mus, './mus.pt')
        torch.save(sigmas, './sigmas.pt')
        torch.save(logpi,'./logpi.pt')
        torch.save(U, './U.pt')
        self.message = message
        super().__init__(self.message)



def gmm_loss(batch, mus, sigmas, logpi,U ):
    """ Computes the gmm loss.

    Compute minus the log probability of batch under the GMM model described
    by mus, sigmas, pi. Precisely, with bs1, bs2, ... the sizes of the batch
    dimensions (several batch dimension are useful when you have both a batch
    axis and a time step axis), gs the number of mixtures and fs the number of
    features.

    :args batch: (bs1, bs2, *, fs) torch tensor
    :args mus: (bs1, bs2, *, gs, fs) torch tensor
    :args sigmas: (bs1, bs2, *, gs, fs) torch tensor
    :args logpi: (bs1, bs2, *, gs) torch tensor
    :args reduce: if not reduce, the mean in the following formula is ommited

    :returns:
    loss(batch) = - mean_{i1=0..bs1, i2=0..bs2, ...} log(
        sum_{k=1..gs} pi[i1, i2, ..., k] * N(
            batch[i1, i2, ..., :] | mus[i1, i2, ..., k, :], sigmas[i1, i2, ..., k, :]))

    """
    batch = batch.unsqueeze(-2)
    losses = torch.zeros(sigmas.size()[0],sigmas.size()[1],sigmas.size()[2]).to(device)
    for seq_num in range(0, sigmas.size()[0]):
        # sequence_loss = torch.zeros(1).to(device)
        for batch_num in range(0, sigmas.size()[1]):
            UUtranspose = torch.bmm(U[seq_num:seq_num+1, batch_num:batch_num+1].view(num_of_guassians,1,latent_space), U[seq_num:seq_num+1, batch_num:batch_num+1].view(num_of_guassians,latent_space,1)).view(num_of_guassians)
            guas_loss = (logpi[seq_num:seq_num + 1, batch_num:batch_num + 1].view(num_of_guassians) -
                (mus.size()[3] / 2) * math.log(2 * math.pi) +
                0.5*torch.log(torch.abs(1+torch.bmm((U[seq_num:seq_num+1, batch_num:batch_num+1].view(num_of_guassians,latent_space)* torch.reciprocal(sigmas[seq_num:seq_num+1, batch_num:batch_num+1]).view(num_of_guassians,latent_space)).view(num_of_guassians,1,latent_space), U[seq_num:seq_num+1, batch_num:batch_num+1].view(num_of_guassians,latent_space,1)).view(num_of_guassians)))+
                0.5 * torch.log(torch.prod(sigmas[seq_num:seq_num + 1, batch_num:batch_num + 1].view(num_of_guassians,latent_space),1)) -
                0.5 * torch.sum((batch[seq_num:seq_num + 1, batch_num:batch_num + 1,0:1] - mus[seq_num:seq_num + 1,batch_num:batch_num + 1]).view(num_of_guassians,latent_space)* sigmas[seq_num:seq_num+1, batch_num:batch_num+1].view(num_of_guassians,1,latent_space)*(batch[seq_num:seq_num + 1, batch_num:batch_num + 1,0:1] - mus[seq_num:seq_num + 1,batch_num:batch_num + 1]).view(num_of_guassians,1,latent_space),dim = (1,2))
                - 0.5 * torch.sum(((batch[seq_num:seq_num + 1, batch_num:batch_num + 1,0:1] - mus[seq_num:seq_num + 1,batch_num:batch_num + 1]).view(num_of_guassians,latent_space)* UUtranspose.view(num_of_guassians,1)).view(num_of_guassians,1,latent_space)*(batch[seq_num:seq_num + 1, batch_num:batch_num + 1,0:1] - mus[seq_num:seq_num + 1,batch_num:batch_num + 1]).view(num_of_guassians,1,latent_space),dim = (1,2))).to(device)

            losses[seq_num:seq_num + 1, batch_num:batch_num + 1,:] = guas_loss
    max_probs = torch.max(losses, dim=-1, keepdim=True)[0]
    losses = losses - max_probs
    g_probs = torch.exp(losses).to(device)
    probs = torch.sum(g_probs, dim=-1).to(device)

    log_prob = (max_probs.squeeze() + torch.log(probs)).to(device)

    final = - torch.mean(log_prob)
    if torch.isnan(final):
        raise NanLossError(batch,mus,sigmas,logpi,U)
    if final.item()<0:
        raise NegativeLossError(batch,mus,sigmas,logpi,U)
    return final


class _MDRNNBase(nn.Module):
    def __init__(self, latents, actions, hiddens, gaussians):
        super().__init__()
        self.latents = latents
        self.actions = actions
        self.hiddens = hiddens
        self.gaussians = gaussians
        self.gmm_linear = nn.Linear(
            hiddens, (3*latents+ 1) * gaussians ) 

    def forward(self, *inputs):
        pass

class MDRNN(_MDRNNBase):
    """ MDRNN model for multi steps forward """
    def __init__(self, latents, actions, hiddens, gaussians):
        super().__init__(latents, actions, hiddens, gaussians)
        self.rnn = nn.LSTM(latents + actions, hiddens)

    def forward(self, actions, latents): 
        """ MULTI STEPS forward.
        :args actions: (SEQ_LEN, BSIZE, ASIZE) torch tensor
        :args latents: (SEQ_LEN, BSIZE, LSIZE) torch tensor

        :returns: mu_nlat, sig_nlat, pi_nlat, rs, ds, parameters of the GMM
        prediction for the next latent, gaussian prediction of the reward and
        logit prediction of terminality.
            - mu_nlat: (SEQ_LEN, BSIZE, N_GAUSS, LSIZE) torch tensor
            - sigma_nlat: (SEQ_LEN, BSIZE, N_GAUSS, LSIZE) torch tensor
            - logpi_nlat: (SEQ_LEN, BSIZE, N_GAUSS) torch tensor
            - rs: (SEQ_LEN, BSIZE) torch tensor
            - ds: (SEQ_LEN, BSIZE) torch tensor
        """
        seq_len, bs = actions.size(0), actions.size(1)

        ins = torch.cat([actions, latents], dim=-1)
        outs, _ = self.rnn(ins)
        gmm_outs = self.gmm_linear(outs)

        stride = self.gaussians * self.latents

        mus = gmm_outs[:, :, :stride]
        mus = mus.view(seq_len, bs, self.gaussians, self.latents)

        sigmas = gmm_outs[:, :, stride:2*stride]
        sigmas = torch.clamp(sigmas.view(seq_len, bs, self.gaussians, self.latents),min=-1 ,max = 1)
        sigmas = torch.exp(sigmas)

        us = gmm_outs[:, :, stride*2:3*stride]
        us = us.view(seq_len, bs, self.gaussians, self.latents)

        pi = gmm_outs[:, :, stride *3: ]
        pi = pi.view(seq_len, bs, self.gaussians)
        logpi = f.log_softmax(pi, dim=-1)


        return mus, sigmas, logpi, us



